#!/bin/sh

echo "Let's beggin the common set up for Kuberenets Lab setup on AWS EC2 instances."
echo "This is common for the all the masters and worker nodes."
echo "Using Kubeadm and using all the default latest versions."
echo "Before starting the installation swap disabled is required."

echo "updating the apt and installing the HTTPS and curl."
sudo apt-get update && sudo apt-get install -y apt-transport-https curl

echo "adding the google gpg key."
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF

echo "update the apt-get again"
sudo apt-get update

echo "installing the docker and kubernets"
sudo apt-get install -y docker.io kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl

sudo systemctl enable kubelet.service
sudo systemctl enable docker.service

cat <<EOF | sudo tee /etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

echo "re-loading the deamon and restarting docker and kubelet"
sudo systemctl daemon-reload
sudo systemctl restart docker
sudo systemctl restart kubelet
